from Kyniv_package.helpers.string import exclamation
from Kyniv_package.helpers.math import calculate_area
print(exclamation(f'The area of 5-by-8 rectangle is {calculate_area(5,8)}'))